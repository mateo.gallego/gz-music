// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  returnURL: 'http://127.0.0.1:4200/home',
  urlAuthorize: 'https://accounts.spotify.com/authorize',
  urlToken: 'https://accounts.spotify.com/api/token',
  urlPlaylist: 'https://api.spotify.com/v1',
  idClient: 'c0e4720b184f4f17a7e1bd81f0160a40',
  secretClient: 'a7d457f8826a4e108faa9ec986d67aea',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
