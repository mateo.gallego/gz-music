import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/pages/login/login.component';

const routes: Routes = [
{ path: 'login', component:LoginComponent
 
},
{
  path: '',  loadChildren: () => import('./components/pages/pages.module').then(n => n.PagesModule),
},
{
  path: '**',redirectTo:''
}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
