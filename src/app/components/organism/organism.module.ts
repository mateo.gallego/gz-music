import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { CardListComponent } from './card-list/card-list.component';
import { AtomsModule } from '../atoms/atoms.module';
import { MoleculesModule } from '../molecules/molecules.module';
import { RouterModule } from '@angular/router';




@NgModule({
  declarations: [
    NavbarComponent,
    CardListComponent
  ],
  imports: [
    CommonModule,
    AtomsModule,
    MoleculesModule,
    RouterModule  
    

  ],
  exports:[
    NavbarComponent,
    CardListComponent


  ]
})
export class OrganismModule { }
