import { Component, Input, OnInit } from '@angular/core';
import { TrackData } from '../../models/models';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit {

  @Input() set cardListInfo(cardArray:TrackData[]){
    this.cardList = cardArray;
    console.log("lista formateada", this.cardList);
  }



  cardList : TrackData[] = [];

  constructor() { }

  ngOnInit(): void {
  }
  

}
