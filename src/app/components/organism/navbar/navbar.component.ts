import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isNavbarOpen:boolean=false;

  ngOnInit(): void {
  }
  constructor(
    private router: Router,
  ) { }
  
  public goPage(type: string): void{
    switch(type){
      case 'FAV':
        this.router.navigate(['/components/pages/favorites']);
        break;
      case 'HOME':
        this.router.navigate(['/components/pages/home']);
    }
  }

  public toggle ():void{
    this.isNavbarOpen=!this.isNavbarOpen
  }

  

  

}
