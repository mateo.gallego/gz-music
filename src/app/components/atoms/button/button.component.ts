import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

 @Input() nameButton!: string;
 @Input() onClick: any;
 @Input() href: string = "";
 @Input() type: number = 0;

 @Output() clickButtonEvent = new EventEmitter<string>();
  

  constructor() { }

  ngOnInit(): void {
  }

  public clickButton(){
    // this.clickButtonEvent.emit();
    this.onClick && this.onClick();
  }

}
