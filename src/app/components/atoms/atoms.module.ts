import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonComponent } from './button/button.component';
import { FigureComponent } from './figure/figure.component';



@NgModule({
  declarations: [
    ButtonComponent,
    FigureComponent,
  ],
  imports: [
    CommonModule,

  ],
  exports:[
    ButtonComponent,
    FigureComponent,
  ]
  
})
export class AtomsModule { }
