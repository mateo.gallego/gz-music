import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, pipe } from 'rxjs';
import { TrackData } from "src/app/components/models/models";
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  urlPrincipalPlayList = environment.urlPlaylist + "/playlists/37i9dQZF1DX7Z7kYpKKGTc"
  urlFavorites = environment.urlPlaylist + "/me/tracks"
  urlProfile = environment.urlPlaylist + "/me"

  constructor(
    private http: HttpClient ,
  ) {}

  getHomePlayList(): Observable<any> {
    const headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sessionStorage.getItem('access_token'),
    })
    return this.http.get<any>(this.urlPrincipalPlayList,{headers})
      
  }

  formatTrack(list: any, isFavorite: boolean=false) :TrackData[] {
   return list.map((elem : any) =>{
     return{
        id: elem.track.id,
        nameTrack: elem.track.name,
        duration: elem.track.duration_ms,
        url: elem.track.external_urls.spotify,
        img: elem.track.album.images[0].url,
        release_date: elem.track.album.release_date,
        album: elem.track.album.name,
        artist: elem.track.album.artists[0].name,
        favorite: isFavorite
     }
   })
  }

  getFavoritesList(): Observable<any>{
    const headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sessionStorage.getItem('access_token'),
    })
    return this.http.get<any>(this.urlFavorites,{headers})
  }

  addFavorite(id: string): Observable<any>{
    const headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sessionStorage.getItem('access_token'),
    })
    const params =new HttpParams()
      .set('ids',id)
    return this.http.put<any>(this.urlFavorites,null,{headers,params})
  }

  removeFavorite(id: string): Observable<any>{
    const headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sessionStorage.getItem('access_token'),
    })
    const params =new HttpParams()
      .set('ids',id)
    return this.http.delete<any>(this.urlFavorites,{headers,params})
  }
















  getFavorites(): Observable<any> {
    return this.http.get<any>(this.urlFavorites);
  }

  putFavorite(id : string): Observable<any> {
    return this.http.put<any>( `${this.urlFavorites}?ids=${id}`, "");
  }

  deleteFavorite(id : string): Observable<any> {
    return this.http.delete<any>( `${this.urlFavorites}?ids=${id}`);
  }

  checkFavoritesGroup(ids: string) : Observable<any> {
    return this.http.get<any>(`${this.urlFavorites}/contains?ids=${ids}`);
  }

  getDataProfile() : Observable<any> {
    return this.http.get<any>(this.urlProfile);
  }
   
}
