import { EventEmitter, Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthToken, userData } from "src/app/components/models/models";
import { CommonService } from 'src/app/components/services/common.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {



  public tokenReady: EventEmitter<boolean> = new EventEmitter<boolean>();
  porfileData: userData ={
    name: '',
    img: ''
  };

  constructor(private _common : CommonService) { }

  public authorizeAccess(): string {
    let url = environment.urlAuthorize;
    url += "?client_id=" + environment.idClient;
    url += "&response_type=token";
    url += "&redirect_uri=" + encodeURI(environment.returnURL);
    url += "&show_dialog=true";
    url += "&scope=user-read-private user-read-email user-modify-playback-state user-read-playback-position user-library-read streaming user-read-playback-state user-read-recently-played playlist-read-private playlist-modify-private playlist-modify user-library-modify";
    // window.location.href = url;
    return url;
  }

   getSplitToken ()  {
    return window.location.hash
      .substring(1)
      .split('&')
      .reduce((prev:any, curr) => {
        const parts : any[] = curr.split('=')
        prev[parts[0]] = decodeURIComponent(parts[1])
        return prev
        
      }, {})
  }

  public handleRedirect(): void {
    console.log("Service");
    let code = this.getCode(window.location.search);
    this.fetchAccessToken(code);    
  }

  private getCode(params: string): string | null {
    let code = null;
    const queryString = params;
    if (queryString.length > 0) {
      const urlParams = new URLSearchParams(queryString);
      code = urlParams.get('code');
    }
    return code;
  }

  private fetchAccessToken(code: any): void {
    const body = new URLSearchParams();
    body.set('grant_type', "authorization_code");
    body.set('code', code);
    body.set('redirect_uri', encodeURI(environment.returnURL));
    body.set('client_id', environment.idClient);
    body.set('client_secret', environment.secretClient);
    this.callAuthorizationApi(body)
  }

  private callAuthorizationApi(body: URLSearchParams): void {
    try {
      let xhr = new XMLHttpRequest();
      xhr.open("POST", environment.urlToken, true);
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhr.setRequestHeader('Authorization', 'Basic ' + btoa(environment.idClient + ":" + environment.secretClient));
      xhr.send(body.toString());
      xhr.onload = (event) =>{
        /* istanbul ignore next */
        this.handleAuthorizationResponse(event.target);
      };
    } catch (error) {
      /* istanbul ignore next */
      console.error(error);
    }
  }

  private handleAuthorizationResponse(res : any) : void {
    console.log(res.status);
    if(res.status == 200){
      let loginResult: AuthToken = JSON.parse(res.responseText);
      if(loginResult.access_token){
        this.setToken(loginResult.access_token);
        this._common.getDataProfile().subscribe(res =>{
          this.infoProfileHandle(res);
          this.tokenReady.emit(true);
        })
      } 
    }
  }
  private infoProfileHandle(res: any): void{
    this.porfileData.name = res.display_name;
    this.porfileData.img = res.images[0].url;
    this.setUser(this.porfileData)
  }

  public setToken(token: string): void{
    sessionStorage.setItem('access_token', token);
  }

  public getToken(): string | null{
    return sessionStorage.getItem('access_token');
  }

  public deleteToken(): void{
    sessionStorage.removeItem('access_token');
  }


  private setUser(data: userData): void{
    sessionStorage.setItem('user_login',JSON.stringify(data));
  }

  public getUser() : any{
    return sessionStorage.getItem('user_login');
  }

  public deleteUser(): void{
    sessionStorage.removeItem('user_login');
  }

}
