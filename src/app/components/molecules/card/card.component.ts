import { Component, Input, OnInit } from '@angular/core';
import { TrackData } from '../../models/models';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() set card(card: TrackData) {
    this.data = card;
  }
  
  data : TrackData = {
    id: '',
    nameTrack: '',
    duration: 0,
    url: '',
    img: '',
    release_date: '',
    album: '',
    artist: '',
    favorite: false
  }

  constructor(
    private commonService: CommonService
  ) { }

  ngOnInit(): void {
  }

  // removeFavorite(id:string){
  //   this.commonService.removeFavorite(id)
  //   .subscribe(console.log)
  // }

  // addFavorite(id: string){
  //   this.commonService.addFavorite(id)
  //   .subscribe(console.log)
  // }

  toggleLike(track:TrackData){
    if(track.favorite){
      this.commonService.removeFavorite(track.id)
      .subscribe(console.log)
    }else{
      this.commonService.addFavorite(track.id)
      .subscribe(console.log)
    }
  }




}
