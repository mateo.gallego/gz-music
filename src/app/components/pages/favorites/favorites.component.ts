import { Component, OnInit } from '@angular/core';
import { TrackData } from '../../models/models';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {

  cardList : TrackData[] = [];

  constructor(
    private authService: AuthService,
    private commonService: CommonService
  ) { }

  ngOnInit(): void {
    this.getFavoriteList()
  }

  getFavoriteList():void{

    this.commonService.getFavoritesList()
      .subscribe(playList => {
        this.cardList=this.commonService.formatTrack(playList.items,true);
        console.log(playList)
      })  
  }

}
