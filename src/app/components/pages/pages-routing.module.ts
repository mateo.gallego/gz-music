import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AtomsModule } from '../atoms/atoms.module';
import { FavoritesComponent } from './favorites/favorites.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path:'',component:LoginComponent},
  {path: 'home', component:HomeComponent},
  {path: 'favorites', component:FavoritesComponent},
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
