import {  NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { HomeComponent } from './home/home.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { OrganismModule } from '../organism/organism.module';
import { MoleculesModule } from '../molecules/molecules.module';


@NgModule({
  declarations: [
    HomeComponent,
    FavoritesComponent
  ],
  imports: [

    CommonModule,
    PagesRoutingModule,
    MoleculesModule,
    OrganismModule,
 

    
  ],

})
export class PagesModule { }
