import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginAutUrl: string = '';

  constructor(
    private _auth: AuthService,
  ) { }
    
  ngOnInit(): void {
    this.loginAutUrl = this._auth.authorizeAccess();
  }

  // public autorizateLogin() : void {
  //   this._auth.authorizeAccess();
  // }

}
