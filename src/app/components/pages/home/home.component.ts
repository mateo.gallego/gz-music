import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { TrackData } from '../../models/models';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  cardList : TrackData[] = [];

  constructor(

    private authService: AuthService,
    private commonService: CommonService
  ) { 
    
  }

  ngOnInit(): void {
    if (window.location.hash){
    const urls = this.authService.getSplitToken()
    this.authService.setToken(urls.access_token)
    window.location.hash = ""
    console.log(urls);
    }
    this.getPlayList()
    
  }

  
  getPlayList():void{

    this.commonService.getHomePlayList()
      
      .subscribe(playList => {
        this.playListResult(playList.tracks.items);
        console.log(playList)

      })  
  }

  private playListResult(list:any):void{
    this.cardList =  this.commonService.formatTrack(list);
  }

 


}
